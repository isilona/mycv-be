package com.isilona.mycv.common;

import com.isilona.mycv.data.entity.FileType;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jboss.resteasy.annotations.providers.multipart.PartType;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MultipartBody {

    @FormParam("data")
    @PartType(MediaType.APPLICATION_OCTET_STREAM)
    private byte[] fileData;

    @FormParam("type")
    private FileType fileType;
}
