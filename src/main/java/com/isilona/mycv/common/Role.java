package com.isilona.mycv.common;

public enum Role {

    FILE_CREATE(File.CREATE);

    private final String authority;

    Role(String authority) {
        this.authority = authority;
    }

    public String getAuthority() {
        return authority;
    }

    public static class File {

        public static final String CREATE = "file:create";

        private File() {
        }
    }

    public static class Knowledge {

        public static final String CREATE = "knowledge:create";
        public static final String UPDATE = "knowledge:update";
        public static final String DELETE = "knowledge:delete";

        private Knowledge() {
        }
    }

    public static class Occupation {

        public static final String CREATE = "occupation:create";
        public static final String UPDATE = "occupation:update";
        public static final String DELETE = "occupation:delete";

        private Occupation() {
        }
    }

    public static class Person {

        public static final String CREATE = "person:create";
        public static final String UPDATE = "person:update";
        public static final String DELETE = "person:delete";

        private Person() {
        }
    }

    public static class Project {

        public static final String CREATE = "project:create";
        public static final String UPDATE = "project:update";
        public static final String DELETE = "project:delete";

        private Project() {
        }
    }

    public static class Template {

        public static final String CREATE = "template:create";
        public static final String UPDATE = "template:update";
        public static final String DELETE = "template:delete";

        private Template() {
        }
    }


}
