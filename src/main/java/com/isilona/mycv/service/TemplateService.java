package com.isilona.mycv.service;

import com.isilona.mycv.data.entity.Template;
import com.isilona.mycv.data.repository.TemplateRepository;
import io.quarkus.panache.common.Sort;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.bson.types.ObjectId;

@ApplicationScoped
public class TemplateService {


    @Inject
    TemplateRepository repository;


    public void create(Template resource) {
        repository.persist(resource);
    }

    public List<Template> read() {
        return repository.listAll();
    }

    public Template readOne(ObjectId id) {
        return repository.findById(id);
    }

    public void update(Template resource) {
        repository.update(resource);
    }

    public void delete(ObjectId id) {
        repository.deleteById(id);
    }

    public Template readLatest() {
        return repository.findAll(Sort.by("id")).firstResult();
    }
}
