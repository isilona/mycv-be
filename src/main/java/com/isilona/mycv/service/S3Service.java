package com.isilona.mycv.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.isilona.mycv.data.entity.File;
import java.io.ByteArrayInputStream;
import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@ApplicationScoped
public class S3Service {

    @ConfigProperty(name = "bucket.name")
    String bucketName;

    @ConfigProperty(name = "bucket.region")
    String clientRegionName;

    @ConfigProperty(name = "bucket.s3AccessKey")
    String s3AccessKey;

    @ConfigProperty(name = "bucket.s3SecretKey")
    String s3SecretKey;

    private AmazonS3 s3Client;


    @PostConstruct
    private void init() {
        BasicAWSCredentials awsCredentials = new BasicAWSCredentials(s3AccessKey, s3SecretKey);
        Regions clientRegion = Regions.fromName(clientRegionName);
        s3Client = AmazonS3ClientBuilder.standard()
            .withRegion(clientRegion)
            .withCredentials(new AWSStaticCredentialsProvider(awsCredentials))
            .build();
    }

    public void putObject(File file, byte[] objectData) {
        s3Client.putObject(bucketName, file.getPath(), new ByteArrayInputStream(objectData), null);
    }

    public S3Object getObject(File file) {
        return s3Client.getObject(new GetObjectRequest(bucketName, file.getPath()));
    }
}
