package com.isilona.mycv.service;

import com.isilona.mycv.data.entity.Knowledge;
import com.isilona.mycv.data.repository.KnowledgeRepository;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.bson.types.ObjectId;

@ApplicationScoped
public class KnowledgeService {

    @Inject
    KnowledgeRepository repository;

    public void create(Knowledge resource) {
        repository.persist(resource);
    }

    public List<Knowledge> read() {
        return repository.listAll();
    }

    public Knowledge readOne(ObjectId id) {
        return repository.findById(id);
    }

    public Knowledge findByPerson(ObjectId personId) {
        return repository.findByPerson(personId);
    }

    public void update(Knowledge resource) {
        repository.update(resource);
    }

    public void delete(ObjectId id) {
        repository.deleteById(id);
    }

}
