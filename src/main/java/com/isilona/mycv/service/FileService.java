package com.isilona.mycv.service;

import com.isilona.mycv.data.entity.File;
import com.isilona.mycv.data.entity.FileType;
import com.isilona.mycv.data.repository.FileRepository;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.bson.types.ObjectId;

@ApplicationScoped
public class FileService {

    @Inject
    FileRepository repository;

    public void create(File resource) {
        repository.persist(resource);
    }

    public File findOneByPersonAndType(ObjectId personId, FileType fileType) {
        return repository.findOneByPersonAndType(personId, fileType);
    }

    public File findOneByPersonAndTypeOrCreateNew(ObjectId personId, FileType fileType, String uid) {
        File foundFile = findOneByPersonAndType(personId, fileType);
        if (foundFile == null) {
            create(File.builder()
                .path(generatePath(uid, fileType))
                .personId(personId)
                .type(fileType)
                .build());
            return findOneByPersonAndType(personId, fileType);
        } else {
            return foundFile;
        }
    }

    private String generatePath(String uid, FileType fileType) {
        return String.format("%s/%s", uid, fileType.name());
    }
}
