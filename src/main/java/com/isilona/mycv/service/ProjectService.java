package com.isilona.mycv.service;

import com.isilona.mycv.data.entity.Project;
import com.isilona.mycv.data.repository.ProjectRepository;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.bson.types.ObjectId;

@ApplicationScoped
public class ProjectService {

    @Inject
    ProjectRepository repository;

    public void create(Project resource) {
        repository.persist(resource);
    }

    public List<Project> read() {
        return repository.listAll();
    }

    public Project readOne(ObjectId id) {
        return repository.findById(id);
    }

    public void update(Project resource) {
        repository.update(resource);
    }

    public void delete(ObjectId id) {
        repository.deleteById(id);
    }

}
