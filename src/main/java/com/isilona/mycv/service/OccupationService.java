package com.isilona.mycv.service;

import com.isilona.mycv.data.entity.Occupation;
import com.isilona.mycv.data.entity.OccupationType;
import com.isilona.mycv.data.repository.OccupationRepository;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.bson.types.ObjectId;

@ApplicationScoped
public class OccupationService {

    @Inject
    OccupationRepository repository;

    public void create(Occupation resource) {
        repository.persist(resource);
    }

    public void update(Occupation resource) {
        repository.update(resource);
    }

    public void delete(ObjectId id) {
        repository.deleteById(id);
    }

    public List<Occupation> findAllByPersonAndType(ObjectId personId, OccupationType occupationType) {
        return (occupationType != null) ?
            repository.findAllByPersonAndType(personId, occupationType) :
            repository.findAllByPerson(personId);
    }
}
