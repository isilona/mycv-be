package com.isilona.mycv.service;

import com.isilona.mycv.data.dto.Token;
import java.util.Base64;
import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@Path("/oauth2/default/v1")
@RegisterRestClient(configKey = "okta-api")
@ApplicationScoped
public interface AuthService {

    @POST
    @Path("/token")
    @Produces(MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = HttpHeaders.AUTHORIZATION, value = "{getAuthorization}")
    @ClientHeaderParam(name = HttpHeaders.CONTENT_TYPE, value = "application/x-www-form-urlencoded")
    Token getToken(
        @QueryParam("grant_type") String grantType,
        @QueryParam("scope") String scope,
        @QueryParam("username") String username,
        @QueryParam("password") String password
    );

    default String getAuthorization() {
        String clientId = ConfigProvider.getConfig().getValue("okta.clientId", String.class);
        String clientSecret = ConfigProvider.getConfig().getValue("okta.clientSecret", String.class);
        return "Basic "
            .concat(
                Base64.getEncoder().encodeToString(
                    clientId.concat(":").concat(clientSecret).getBytes()
                )
            );
    }
}
