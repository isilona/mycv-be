package com.isilona.mycv.service;

import com.isilona.mycv.data.entity.Person;
import com.isilona.mycv.data.repository.PersonRepository;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import org.bson.types.ObjectId;

@ApplicationScoped
public class PersonService {

    @Inject
    PersonRepository repository;

    public void create(Person resource) {
        repository.persist(resource);
    }

    public List<Person> read() {
        return repository.listAll();
    }

    public Person readOne(ObjectId id) {
        Person result = repository.findById(id);
        result.setAge(calculateAge(result.getDateOfBirth()));
        result.setDateOfBirth(null);
        return result;
    }

    public void update(Person resource) {
        repository.update(resource);
    }

    public void delete(ObjectId id) {
        repository.deleteById(id);
    }

    private long calculateAge(LocalDate localDate) {

        return ChronoUnit.YEARS.between(localDate, LocalDate.now());
    }
}
