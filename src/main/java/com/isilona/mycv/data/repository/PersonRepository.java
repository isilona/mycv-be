package com.isilona.mycv.data.repository;

import com.isilona.mycv.data.entity.Person;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PersonRepository implements PanacheMongoRepository<Person> {

}
