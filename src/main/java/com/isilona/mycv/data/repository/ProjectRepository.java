package com.isilona.mycv.data.repository;

import com.isilona.mycv.data.entity.Project;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ProjectRepository implements PanacheMongoRepository<Project> {

}
