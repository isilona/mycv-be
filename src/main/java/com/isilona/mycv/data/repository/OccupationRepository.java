package com.isilona.mycv.data.repository;

import com.isilona.mycv.data.entity.Occupation;
import com.isilona.mycv.data.entity.OccupationType;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.quarkus.panache.common.Parameters;
import io.quarkus.panache.common.Sort;
import java.util.List;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@ApplicationScoped
public class OccupationRepository implements PanacheMongoRepository<Occupation> {

    public List<Occupation> findAllByPersonAndType(ObjectId personId, OccupationType occupationType) {
        return find("{personId : :personId, type : :occupationType}",
            Sort.descending("startDate"),
            Parameters.with("personId", personId)
                .and("occupationType", occupationType))
            .list();
    }

    public List<Occupation> findAllByPerson(ObjectId personId) {
        return find("{personId : :personId}",
            Sort.descending("startDate"),
            Parameters.with("personId", personId))
            .list();
    }
}
