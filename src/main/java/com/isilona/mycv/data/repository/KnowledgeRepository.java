package com.isilona.mycv.data.repository;

import com.isilona.mycv.data.entity.Knowledge;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.quarkus.panache.common.Parameters;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@ApplicationScoped
public class KnowledgeRepository implements PanacheMongoRepository<Knowledge> {

    public Knowledge findByPerson(ObjectId personId) {
        return find("{personId : :personId}",
            Parameters.with("personId", personId))
            .firstResult();
    }
}
