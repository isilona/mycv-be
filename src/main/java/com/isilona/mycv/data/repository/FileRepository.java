package com.isilona.mycv.data.repository;

import com.isilona.mycv.data.entity.File;
import com.isilona.mycv.data.entity.FileType;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.quarkus.panache.common.Parameters;
import javax.enterprise.context.ApplicationScoped;
import org.bson.types.ObjectId;

@ApplicationScoped
public class FileRepository implements PanacheMongoRepository<File> {

    public File findOneByPersonAndType(ObjectId personId, FileType fileType) {
        return find("personId = :personId, type = :fileType",
            Parameters
                .with("personId", personId)
                .and("fileType", fileType.name())
                .map())
            .firstResult();
    }

}
