package com.isilona.mycv.data.repository;

import com.isilona.mycv.data.entity.Template;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class TemplateRepository implements PanacheMongoRepository<Template> {

}
