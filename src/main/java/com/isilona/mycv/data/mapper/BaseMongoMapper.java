package com.isilona.mycv.data.mapper;

import org.bson.types.ObjectId;

public interface BaseMongoMapper {

    default String objectIdToString(ObjectId source) {
        if (source != null) {
            return source.toString();
        }
        return null;
    }

    default ObjectId objectIdToString(String source) {
        return new ObjectId(source);
    }

}
