package com.isilona.mycv.data.mapper;

import com.isilona.mycv.data.dto.OccupationDto;
import com.isilona.mycv.data.entity.Occupation;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "CDI")
public interface OccupationMapper extends BaseMongoMapper {

    @Mapping(target = "endDate", dateFormat = "MMM yyyy", defaultValue = "Present")
    @Mapping(target = "startDate", dateFormat = "MMM yyyy")
    OccupationDto toDto(Occupation source);

}
