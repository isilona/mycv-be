package com.isilona.mycv.data.entity;

import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Knowledge {

    private ObjectId id;

    //    <SkillType, SkillName>
    private Map<String, List<String>> skillBuckets;

    // relations
    private ObjectId personId;
}
