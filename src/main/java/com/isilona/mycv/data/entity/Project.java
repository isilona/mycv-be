package com.isilona.mycv.data.entity;

import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Project {

    private ObjectId id;
    private String name;
    private Integer orderNum;
    private String description;
    private String shortDescription;
    private String gitUrl;
    private String liveUrl;
    private Set<Gain> gains;

    // relations
    private ObjectId personId;
    private ObjectId occupationId;


    @Getter
    @Setter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Gain {

        private String description;
    }

}
