package com.isilona.mycv.data.entity;

import java.time.LocalDate;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Occupation {

    private ObjectId id;
    private String position;
    private Company company;
    private LocalDate startDate;
    private LocalDate endDate;
    private OccupationType type;
    private String description;

    // relations
    private ObjectId personId;


    @Getter
    @Setter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Company {

        private String name;
        private String url;
    }

}
