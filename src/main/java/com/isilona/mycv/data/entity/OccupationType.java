package com.isilona.mycv.data.entity;

public enum OccupationType {
    WRK,
    EDU,
    HOME
}
