package com.isilona.mycv.data.entity;

import io.quarkus.mongodb.panache.MongoEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@MongoEntity(collection = "files")
public class File {

    private ObjectId id;
    private String path;
    private FileType type;

    // relations
    private ObjectId personId;

}
