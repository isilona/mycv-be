package com.isilona.mycv.data.entity;

import java.time.LocalDate;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.bson.types.ObjectId;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Person {

    private ObjectId id;
    // Index
    private String firstName;
    private String lastName;
    private Title title;
    private Set<SocialLink> socialLinks;
    // About me
    private String shortDetails;
    private LocalDate dateOfBirth;
    private Long age;
    private Address address;
    private Contact contact;
    private Set<Ability> abilities;
    // Resume / experience


    @Getter
    @Setter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Title {

        private String base;
        private Set<String> subtitles;
    }


    @Getter
    @Setter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Address {

        private String country;
        private String city;
        private String postCode;
    }

    @Getter
    @Setter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Contact {

        private String phone;
        private String email;
    }

    @Getter
    @Setter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Ability {

        private String title;
        private String faClass;
        private String description;
    }

    @Getter
    @Setter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SocialLink {

        private String faClass;
        private String url;
    }
}
