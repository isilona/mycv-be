package com.isilona.mycv.data.dto;

import javax.json.bind.annotation.JsonbProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Token {

    @JsonbProperty("token_type")
    private String tokenType;

    @JsonbProperty("expires_in")
    private Integer expiresIn;

    @JsonbProperty("access_token")
    private String accessToken;

    private String scope;

    @JsonbProperty("id_token")
    private String idToken;

}
