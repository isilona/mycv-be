package com.isilona.mycv.data.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OccupationDto {

    private String id;
    private String position;
    private CompanyDto company;
    private String startDate;
    private String endDate;
    private String type;
    private String description;

    // relations
    private String personId;


    @Getter
    @Setter
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CompanyDto {

        private String name;
        private String url;
    }

}
