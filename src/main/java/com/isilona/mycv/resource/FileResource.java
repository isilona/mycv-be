package com.isilona.mycv.resource;

import com.amazonaws.services.s3.model.S3Object;
import com.isilona.mycv.common.MultipartBody;
import com.isilona.mycv.common.Role;
import com.isilona.mycv.data.entity.File;
import com.isilona.mycv.data.entity.FileType;
import com.isilona.mycv.service.FileService;
import com.isilona.mycv.service.S3Service;
import java.util.Set;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import lombok.extern.jbosslog.JBossLog;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.jwt.Claim;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;

@JBossLog
@Path("/files")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class FileResource {

    @Inject
    S3Service s3Service;

    @Inject
    FileService service;

    @Inject
    @Claim("groups")
    Set<String> groups;

    @Inject
    @Claim("uid")
    String uid;

    @POST
    @Path("/{personId}")
    @RolesAllowed(Role.File.CREATE)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public void create(@PathParam ObjectId personId, @MultipartForm MultipartBody form) {
        File file = service.findOneByPersonAndTypeOrCreateNew(personId, form.getFileType(), uid);
        s3Service.putObject(file, form.getFileData());
    }


    @GET
    @Path("/{personId}/{fileType}")
    @Produces("image/png")
    public Response read(@PathParam ObjectId personId, @PathParam FileType fileType) {
        File file = service.findOneByPersonAndType(personId, fileType);
        S3Object object = s3Service.getObject(file);
        return Response.ok()
            .entity(object.getObjectContent()).build();
    }

}
