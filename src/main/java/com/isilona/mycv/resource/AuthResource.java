package com.isilona.mycv.resource;

import com.isilona.mycv.data.dto.Token;
import com.isilona.mycv.service.AuthService;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.jboss.resteasy.annotations.jaxrs.QueryParam;

@Path("/auth")
@RequestScoped
public class AuthResource {

    @Inject
    @RestClient
    AuthService service;

    @POST()
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    public Token login(@QueryParam String username, @QueryParam String password) {
        return service.getToken("password", "openid", username, password);
    }
}
