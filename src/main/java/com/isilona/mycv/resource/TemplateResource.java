package com.isilona.mycv.resource;

import com.isilona.mycv.common.Role;
import com.isilona.mycv.data.entity.Template;
import com.isilona.mycv.service.TemplateService;
import java.util.List;
import java.util.Set;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.jwt.Claim;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@Path("/templates")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TemplateResource {

    @Inject
    TemplateService service;

    @Inject
    @Claim("groups")
    Set<String> groups;

    @POST
    @RolesAllowed(Role.Template.CREATE)
    public void create(Template resource) {
        service.create(resource);
    }

    @GET
    public List<Template> read() {
        return service.read();
    }

    @GET
    @Path("/{id}")
    public Template readOne(@PathParam ObjectId id) {
        return service.readOne(id);
    }

    @PUT
    @RolesAllowed(Role.Template.UPDATE)
    public void update(Template resource) {
        service.update(resource);
    }

    @DELETE
    @Path("/{id}")
    @RolesAllowed(Role.Template.DELETE)
    public void delete(@PathParam ObjectId id) {
        service.delete(id);
    }
}
