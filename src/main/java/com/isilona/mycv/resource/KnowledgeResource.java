package com.isilona.mycv.resource;


import com.isilona.mycv.common.Role;
import com.isilona.mycv.data.entity.Knowledge;
import com.isilona.mycv.service.KnowledgeService;
import java.util.Set;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lombok.extern.jbosslog.JBossLog;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.jwt.Claim;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@JBossLog
@Path("/knowledge")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class KnowledgeResource {

    @Inject
    KnowledgeService service;

    @Inject
    @Claim("groups")
    Set<String> groups;

    @POST
    @RolesAllowed(Role.Knowledge.CREATE)
    public void create(Knowledge resource) {
        service.create(resource);
    }

    @GET
    @Path("/{personId}")
    public Knowledge findAllByPerson(@PathParam ObjectId personId) {
        log.infov("personId:{0}", personId);
        return service.findByPerson(personId);
    }

    @PUT
    @RolesAllowed(Role.Knowledge.UPDATE)
    public void update(Knowledge resource) {
        service.update(resource);
    }

    @DELETE
    @Path("/{id}")
    @RolesAllowed(Role.Knowledge.DELETE)
    public void delete(@PathParam ObjectId id) {
        service.delete(id);
    }

}
