package com.isilona.mycv.resource;

import com.isilona.mycv.common.Role;
import com.isilona.mycv.data.dto.OccupationDto;
import com.isilona.mycv.data.entity.Occupation;
import com.isilona.mycv.data.entity.OccupationType;
import com.isilona.mycv.data.mapper.OccupationMapper;
import com.isilona.mycv.service.OccupationService;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lombok.extern.jbosslog.JBossLog;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.jwt.Claim;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@JBossLog
@Path("/occupations")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class OccupationResource {

    @Inject
    OccupationService service;

    @Inject
    OccupationMapper occupationMapper;

    @Inject
    @Claim("groups")
    Set<String> groups;

    @POST
    @RolesAllowed(Role.Occupation.CREATE)
    public void create(Occupation resource) {
        service.create(resource);
    }

    @GET
    @Path("/{personId}/{occupationType}")
    public List<OccupationDto> findAllByPersonAndType(@PathParam ObjectId personId, @PathParam OccupationType occupationType) {
        log.infov("personId:{0}", personId);
        log.infov("occupationType:{0}", occupationType);
        return service.findAllByPersonAndType(personId, occupationType).stream()
            .map(occupationMapper::toDto)
            .collect(Collectors.toList());
    }

    @GET
    @Path("/{personId}")
    public List<OccupationDto> findAllByPerson(@PathParam ObjectId personId) {
        log.infov("personId:{0}", personId);
        return service.findAllByPersonAndType(personId, null).stream()
            .map(occupationMapper::toDto)
            .collect(Collectors.toList());
    }

    @PUT
    @RolesAllowed(Role.Occupation.UPDATE)
    public void update(Occupation resource) {
        service.update(resource);
    }

    @DELETE
    @Path("/{id}")
    @RolesAllowed(Role.Occupation.DELETE)
    public void delete(@PathParam ObjectId id) {
        service.delete(id);
    }

}
