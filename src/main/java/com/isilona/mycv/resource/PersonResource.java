package com.isilona.mycv.resource;

import com.isilona.mycv.common.Role;
import com.isilona.mycv.data.entity.Person;
import com.isilona.mycv.service.PersonService;
import java.util.List;
import java.util.Set;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lombok.extern.jbosslog.JBossLog;
import org.bson.types.ObjectId;
import org.eclipse.microprofile.jwt.Claim;
import org.jboss.resteasy.annotations.jaxrs.PathParam;

@JBossLog
@Path("/persons")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class PersonResource {

    @Inject
    PersonService service;

    @Inject
    @Claim("groups")
    Set<String> groups;

    @POST
    @RolesAllowed(Role.Person.CREATE)
    public void create(Person person) {
        service.create(person);
    }

    @GET
    public List<Person> read() {
        log.info("GET : /persons");
        return service.read();
    }

    @GET
    @Path("/{id}")
    public Person readOne(@PathParam ObjectId id) {
        return service.readOne(id);
    }

    @PUT
    @RolesAllowed(Role.Person.UPDATE)
    public void update(Person person) {
        log.info(person);
        service.update(person);
    }

    @DELETE
    @Path("/{id}")
    @RolesAllowed(Role.Person.DELETE)
    public void delete(@PathParam ObjectId id) {
        service.delete(id);
    }

}
