package com.isilona.mycv.resource;

import static com.itextpdf.text.pdf.BaseFont.EMBEDDED;
import static com.itextpdf.text.pdf.BaseFont.IDENTITY_H;
import static org.thymeleaf.templatemode.TemplateMode.HTML;

import com.isilona.mycv.data.dto.OccupationDto;
import com.isilona.mycv.data.entity.Knowledge;
import com.isilona.mycv.data.entity.OccupationType;
import com.isilona.mycv.data.entity.Person;
import com.isilona.mycv.data.mapper.OccupationMapper;
import com.isilona.mycv.service.KnowledgeService;
import com.isilona.mycv.service.OccupationService;
import com.isilona.mycv.service.PersonService;
import com.itextpdf.text.DocumentException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.bson.types.ObjectId;
import org.jboss.resteasy.annotations.jaxrs.PathParam;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

@Path("/transform")
public class TransformationResource {

    private static final String UTF_8 = "UTF-8";

    @Inject
    OccupationService occupationService;

    @Inject
    KnowledgeService knowledgeService;

    @Inject
    PersonService personService;

    @Inject
    OccupationMapper occupationMapper;

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response transform(@PathParam ObjectId id) throws IOException, DocumentException {
        File generatedFile = generatePdf(id, false);
        return Response
            .ok(generatedFile)
            .header("Content-Disposition", String.format("attachment;filename=Iskren_Ivanov_CV_%s.pdf", LocalDate.now().toString()))
            .build();
    }

    @GET
    @Path("/{id}/blind")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response transformBlind(@PathParam ObjectId id) throws IOException, DocumentException {
        File generatedFile = generatePdf(id, true);
        return Response
            .ok(generatedFile)
            .header("Content-Disposition", String.format("attachment;filename=Java_BE_developer_CV_%s.pdf", LocalDate.now().toString()))
            .build();
    }

    private File generatePdf(ObjectId personId, boolean blind) throws IOException, DocumentException {

        // We set-up a Thymeleaf rendering engine. All Thymeleaf templates
        // are HTML-based files located under "src/test/resources". Beside
        // of the main HTML file, we also have partials like a footer or
        // a header. We can re-use those partials in different documents.
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("/META-INF/resources/pdfTemplates/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(HTML);
        templateResolver.setCharacterEncoding(UTF_8);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        // The data in our Thymeleaf templates is not hard-coded. Instead,
        // we use placeholders in our templates. We fill these placeholders
        // with actual data by passing in an object. In this example, we will
        // write a letter to "John Doe".
        //
        // Note that we could also read this data from a JSON file, a database
        // a web service or whatever.

        List<OccupationDto> experienceData = occupationService
            .findAllByPersonAndType(personId, null)
            .stream()
            .filter(occupation -> occupation.getType().equals(OccupationType.WRK))
            .map(occupationMapper::toDto)
            .collect(Collectors.toList());

        List<OccupationDto> educationData = occupationService
            .findAllByPersonAndType(personId, null)
            .stream()
            .filter(occupation -> occupation.getType().equals(OccupationType.EDU))
            .map(occupationMapper::toDto)
            .collect(Collectors.toList());

        Knowledge knowledgeData = knowledgeService.findByPerson(personId);
        Person personalData = personService.readOne(personId);

        Context context = new Context();
        context.setVariable("experienceData", experienceData);
        context.setVariable("educationData", educationData);
        context.setVariable("knowledgeData", knowledgeData);
        context.setVariable("personalData", personalData);

        // Flying Saucer needs XHTML - not just normal HTML. To make our life
        // easy, we use JTidy to convert the rendered Thymeleaf template to
        // XHTML. Note that this might not work for very complicated HTML. But
        // it's good enough for a simple letter.
        String renderedHtmlContent;
        if (blind) {
            renderedHtmlContent = templateEngine.process("blindtemplate", context);
        } else {
            renderedHtmlContent = templateEngine.process("template", context);
        }

        String xHtml = convertToXhtml(renderedHtmlContent);

        ITextRenderer renderer = new ITextRenderer();
        renderer.getFontResolver().addFont("META-INF/resources/pdfTemplates/Code39.ttf", IDENTITY_H, EMBEDDED);

        renderer.setDocumentFromString(xHtml);
        renderer.layout();

        File temp = File.createTempFile("tempfile", ".tmp");

        // And finally, we create the PDF:
        OutputStream outputStream = new FileOutputStream(temp);
        renderer.createPDF(outputStream);
        outputStream.close();

        return temp;
    }

    private String convertToXhtml(String html) {
        Tidy tidy = new Tidy();
        tidy.setInputEncoding(UTF_8);
        tidy.setOutputEncoding(UTF_8);
        tidy.setXHTML(true);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        tidy.parseDOM(inputStream, outputStream);
        return outputStream.toString(StandardCharsets.UTF_8);
    }

}
