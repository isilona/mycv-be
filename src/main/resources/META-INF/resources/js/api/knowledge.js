$(document).ready(function fillOccupationsPage() {
  $.getJSON("/knowledge/5fb30387f79cbf74995ae686")
  .success(function (knowledges) {

    const Item = ({value, key}) => `
        <div class="skill clearfix">
          <h4>${key}</h4>
        </div>
        <ul class="knowledges">
        </ul>
        </br>
    `;

    const map = new Map(Object.entries(knowledges.skillBuckets));

    map.forEach(function (value, key) {

      $("#myCv-resume-knowledge").append(
          [{key: key, value: value}].map(Item).join(''))

      value.forEach((function (item) {
        $("#myCv-resume-knowledge>ul:last").append(`<li>${item}</li>`)
      }))
    })
  })
  .error(function () {
    console.error(occupations)
  });
});
