$(document).ready(function fillIndexPage() {
  $.getJSON("/persons/5fb30387f79cbf74995ae686")
  .success(function (person) {
    // Index
    $("#myCv-index-name").text(person.firstName + " " + person.lastName);
    $("#myCv-index-title").text(person.title.base);
    person.socialLinks.forEach(function (item) {
      $("#myCv-index-social-links").append(
          `<li><a href=\"${item.url}\" target=\"_blank\"><i class=\"${item.faClass}\"></i></a></li>`
      )
    });
    $("#myCv-index-profile-image").attr("alt",
        person.firstName + " " + person.lastName);
    $("#myCv-index-h2-title").text(person.firstName + " " + person.lastName);

    // About me
    $("#myCv-aboutMe-shortDetails").text(person.shortDetails);
    // $("#myCv-aboutMe-age").text(person.age);
    $("#myCv-aboutMe-residence").text(person.address.country);
    $("#myCv-aboutMe-address").text(
        person.address.postCode + ", " + person.address.city);
    $("#myCv-aboutMe-email").text(person.contact.email);
    $("#myCv-aboutMe-phone").text(person.contact.phone);

    $(".myCv-aboutMe-h4-title").each(function (index) {
      $(this).text(person.abilities[index].title);
    })
    $(".myCv-aboutMe-p-description").each(function (index) {
      $(this).text(person.abilities[index].description);
    })
    $(".myCv-aboutMe-i-faClass").each(function (index) {
      $(this).addClass(person.abilities[index].faClass);
      console.log(person.abilities[index].faClass)
    })
  })
  .error(function () {
    console.error(person)
  });
});