$(document).ready(function fillOccupationsPage() {
  $.getJSON("/occupations/5fb30387f79cbf74995ae686")
  .success(function (occupations) {

    const Item = ({occupation}) => `
    <div class="timeline-item clearfix">
        <div class="left-part">
        <h5 class="item-period">
          ${occupation.startDate} <br/> ${occupation.endDate}
        </h5>
        <span class="item-company">${occupation.company.name}</span>
    </div>
    <div class="divider"></div>
        <div class="right-part">
        <h4 class="item-title">${occupation.position}</h4>
    <p>${occupation.description}</p>
    </div>
    </div>
    `;

    occupations.filter(
        function (occupation) {
          return occupation.type === 'EDU'
        }
    ).forEach(function (item) {
      $("#myCv-resume-education").append(
          [{occupation: item}].map(Item).join(''))
    })

    occupations.filter(
        function (occupation) {
          return occupation.type === 'WRK'
        }
    ).forEach(function (item) {
      $("#myCv-resume-experience").append(
          [{occupation: item}].map(Item).join(''))
    })

  })
  .error(function () {
    console.error(occupations)
  });
});