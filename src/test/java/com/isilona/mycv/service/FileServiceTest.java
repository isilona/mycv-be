package com.isilona.mycv.service;

import static com.isilona.mycv.data.entity.FileType.PROFILE_IMG;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.entity.File;
import com.isilona.mycv.data.entity.FileType;
import com.isilona.mycv.data.repository.FileRepository;
import com.isilona.mycv.resource.base.MockProfile;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import java.util.UUID;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestProfile(MockProfile.class)
class FileServiceTest extends BaseFaker {

    @Inject
    FileService service;

    @InjectMock
    FileRepository repository;

    @Test
    void create() {
        File entityUnderTest = File.builder().build();
        service.create(entityUnderTest);

        Mockito.verify(repository, Mockito.times(1)).persist(entityUnderTest);

    }

    @Test
    void findOneByPersonAndType() {
        ObjectId objectId = ObjectId.get();
        File expected = File.builder()
            .path(faker.file().fileName())
            .build();

        Mockito.when(repository.findOneByPersonAndType(objectId, PROFILE_IMG))
            .thenReturn(expected);

        File result = service.findOneByPersonAndType(objectId, PROFILE_IMG);

        assertEquals(expected.getPath(), result.getPath());
        Mockito.verify(repository, Mockito.times(1)).findOneByPersonAndType(objectId, PROFILE_IMG);
    }

    @Test
    void findOneByPersonAndTypeOrCreateNew_findOne() {
        ObjectId personId = ObjectId.get();
        UUID uid = UUID.randomUUID();

        File expected = File.builder()
            .path(faker.file().fileName())
            .build();

        Mockito.when(repository.findOneByPersonAndType(personId, PROFILE_IMG))
            .thenReturn(expected);

        File result = service.findOneByPersonAndTypeOrCreateNew(personId, PROFILE_IMG, uid.toString());

        assertEquals(expected.getPath(), result.getPath());

        Mockito.verify(repository, Mockito.times(1)).findOneByPersonAndType(personId, PROFILE_IMG);
        Mockito.verify(repository, Mockito.never()).persist(any(File.class));
    }

    @Test
    void findOneByPersonAndTypeOrCreateNew_createNew() {

        ObjectId personId = ObjectId.get();
        UUID uid = UUID.randomUUID();
        FileType fileType = PROFILE_IMG;

        File entityUnderTest = File.builder()
            .path(String.format("%s/%s", uid, fileType.name()))
            .personId(personId)
            .type(fileType)
            .build();

        Mockito.when(repository.findOneByPersonAndType(personId, PROFILE_IMG))
            .thenReturn(null);

        service.findOneByPersonAndTypeOrCreateNew(personId, fileType, uid.toString());

        Mockito.verify(repository, Mockito.times(2)).findOneByPersonAndType(personId, PROFILE_IMG);
        Mockito.verify(repository, Mockito.times(1)).persist(eq(entityUnderTest));

    }
}