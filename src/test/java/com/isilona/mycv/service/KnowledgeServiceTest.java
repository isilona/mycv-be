package com.isilona.mycv.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.entity.Knowledge;
import com.isilona.mycv.data.repository.KnowledgeRepository;
import com.isilona.mycv.resource.base.MockProfile;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import java.util.List;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestProfile(MockProfile.class)
class KnowledgeServiceTest extends BaseFaker {

    @Inject
    KnowledgeService service;

    @InjectMock
    KnowledgeRepository repository;

    @Test
    void create() {

        Knowledge entityUnderTest = Knowledge.builder().build();

        service.create(entityUnderTest);
        assertNull(entityUnderTest.getSkillBuckets());
        Mockito.verify(repository, Mockito.times(1)).persist(any(Knowledge.class));


    }

    @Test
    void read() {

        Mockito.when(repository.listAll()).thenReturn(List.of(
            Knowledge.builder()
                .personId(ObjectId.get())
                .id(ObjectId.get())
                .build()
        ));

        List<Knowledge> result = service.read();
        assertEquals(1, result.size());
        assertNotNull(result.get(0).getPersonId());
        assertNotNull(result.get(0).getId());
        assertNull(result.get(0).getSkillBuckets());

        Mockito.verify(repository, Mockito.times(1)).listAll();
    }

    @Test
    void readOne() {

        ObjectId personId = ObjectId.get();
        ObjectId id = ObjectId.get();

        Mockito.when(repository.findById(any(ObjectId.class))).thenReturn(Knowledge.builder()
            .personId(personId)
            .id(id)
            .build());

        Knowledge result = service.readOne(ObjectId.get());
        assertEquals(personId, result.getPersonId());
        assertEquals(id, result.getId());
    }

    @Test
    void findByPerson() {

        ObjectId personId = ObjectId.get();
        ObjectId id = ObjectId.get();

        Mockito.when(repository.findByPerson(personId)).thenReturn(Knowledge.builder()
            .personId(personId)
            .id(id)
            .build());

        Knowledge result = service.findByPerson(personId);
        assertEquals(personId, result.getPersonId());
        assertEquals(id, result.getId());
    }

    @Test
    void update() {
        Knowledge entityUnderTest = Knowledge.builder()
            .id(ObjectId.get())
            .personId(ObjectId.get())
            .build();

        service.update(entityUnderTest);
        Mockito.verify(repository, Mockito.times(1)).update(any(Knowledge.class));

    }

    @Test
    void delete() {
        service.delete(ObjectId.get());
        Mockito.verify(repository, Mockito.times(1)).deleteById(any(ObjectId.class));

    }
}