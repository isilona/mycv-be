package com.isilona.mycv.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.entity.Project;
import com.isilona.mycv.data.repository.ProjectRepository;
import com.isilona.mycv.resource.base.MockProfile;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import java.util.List;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestProfile(MockProfile.class)
class ProjectServiceTest extends BaseFaker {

    @Inject
    ProjectService service;

    @InjectMock
    ProjectRepository repository;

    @Test
    void create() {
        Project entityUnderTest = Project.builder().build();

        service.create(entityUnderTest);
        assertNull(entityUnderTest.getDescription());
        Mockito.verify(repository, Mockito.times(1)).persist(any(Project.class));
    }

    @Test
    void create_data_not_changed() {
        String projectName = getProjectName();
        Project entityUnderTest = Project.builder()
            .name(projectName)
            .build();

        service.create(entityUnderTest);
        assertEquals(projectName, entityUnderTest.getName());
        assertNull(entityUnderTest.getDescription());
        Mockito.verify(repository, Mockito.times(1)).persist(any(Project.class));
    }

    @Test
    void read() {
        String projectName = getProjectName();
        Mockito.when(repository.listAll()).thenReturn(List.of(
            Project.builder()
                .name(projectName)
                .build()
        ));

        List<Project> result = service.read();
        assertEquals(1, result.size());
        assertEquals(projectName, result.get(0).getName());

        Mockito.verify(repository, Mockito.times(1)).listAll();
    }

    @Test
    void readOne() {
        String projectName = getProjectName();
        Mockito.when(repository.findById(any(ObjectId.class))).thenReturn(Project.builder()
            .name(projectName)
            .build());

        Project result = service.readOne(ObjectId.get());
        assertEquals(projectName, result.getName());

    }

    @Test
    void update() {
        String projectName = getProjectName();
        Project entityUnderTest = Project.builder()
            .id(ObjectId.get())
            .name(projectName)
            .build();

        service.update(entityUnderTest);
        Mockito.verify(repository, Mockito.times(1)).update(any(Project.class));

    }

    @Test
    void delete() {
        service.delete(ObjectId.get());
        Mockito.verify(repository, Mockito.times(1)).deleteById(any(ObjectId.class));
    }
}