package com.isilona.mycv.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.entity.Template;
import com.isilona.mycv.data.repository.TemplateRepository;
import com.isilona.mycv.resource.base.MockProfile;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import java.util.List;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestProfile(MockProfile.class)
class TemplateServiceTest extends BaseFaker {

    @Inject
    TemplateService service;

    @InjectMock
    TemplateRepository repository;

    @Test
    void create() {
        Template entityUnderTest = Template.builder().build();

        service.create(entityUnderTest);
        assertNull(entityUnderTest.getValue());
        Mockito.verify(repository, Mockito.times(1)).persist(any(Template.class));
    }

    @Test
    void create_data_not_changed() {
        String templateValue = getLorem(10);
        Template entityUnderTest = Template.builder()
            .value(templateValue)
            .build();

        service.create(entityUnderTest);
        assertEquals(templateValue, entityUnderTest.getValue());
        Mockito.verify(repository, Mockito.times(1)).persist(any(Template.class));
    }

    @Test
    void read() {
        String templateValue = getLorem(10);
        Mockito.when(repository.listAll()).thenReturn(List.of(
            Template.builder()
                .value(templateValue)
                .build()
        ));

        List<Template> result = service.read();
        assertEquals(1, result.size());
        assertEquals(templateValue, result.get(0).getValue());

        Mockito.verify(repository, Mockito.times(1)).listAll();
    }

    @Test
    void readOne() {
        String templateValue = getLorem(10);
        Mockito.when(repository.findById(any(ObjectId.class))).thenReturn(Template.builder()
            .value(templateValue)
            .build());

        Template result = service.readOne(ObjectId.get());
        assertEquals(templateValue, result.getValue());

    }

    @Test
    void update() {
        String templateValue = getLorem(10);
        Template entityUnderTest = Template.builder()
            .id(ObjectId.get())
            .value(templateValue)
            .build();

        service.update(entityUnderTest);
        Mockito.verify(repository, Mockito.times(1)).update(any(Template.class));

    }

    @Test
    void delete() {
        service.delete(ObjectId.get());
        Mockito.verify(repository, Mockito.times(1)).deleteById(any(ObjectId.class));
    }
}