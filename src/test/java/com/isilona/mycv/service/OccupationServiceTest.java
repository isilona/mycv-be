package com.isilona.mycv.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.entity.Occupation;
import com.isilona.mycv.data.repository.OccupationRepository;
import com.isilona.mycv.resource.base.MockProfile;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestProfile(MockProfile.class)
class OccupationServiceTest extends BaseFaker {

    @Inject
    OccupationService service;

    @InjectMock
    OccupationRepository repository;

    @Test
    void create() {
        Occupation entityUnderTest = Occupation.builder().build();

        service.create(entityUnderTest);
        assertNull(entityUnderTest.getPosition());
        Mockito.verify(repository, Mockito.times(1)).persist(any(Occupation.class));
    }

    @Test
    void create_data_not_changed() {
        String position = faker.job().position();
        Occupation entityUnderTest = Occupation.builder()
            .position(position)
            .build();

        service.create(entityUnderTest);
        assertEquals(position, entityUnderTest.getPosition());
        assertNull(entityUnderTest.getDescription());
        Mockito.verify(repository, Mockito.times(1)).persist(any(Occupation.class));
    }

    @Test
    void update() {
        String position = faker.job().position();
        Occupation entityUnderTest = Occupation.builder()
            .id(ObjectId.get())
            .position(position)
            .build();

        service.update(entityUnderTest);
        Mockito.verify(repository, Mockito.times(1)).update(any(Occupation.class));

    }

    @Test
    void delete() {
        service.delete(ObjectId.get());
        Mockito.verify(repository, Mockito.times(1)).deleteById(any(ObjectId.class));
    }
}