package com.isilona.mycv.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.any;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.entity.Person;
import com.isilona.mycv.data.repository.PersonRepository;
import com.isilona.mycv.resource.base.MockProfile;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import java.time.LocalDate;
import java.time.Month;
import java.util.List;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestProfile(MockProfile.class)
class PersonServiceTest extends BaseFaker {

    @Inject
    PersonService service;

    @InjectMock
    PersonRepository repository;

    @Test
    void create() {
        Person entityUnderTest = Person.builder().build();

        service.create(entityUnderTest);
        assertNull(entityUnderTest.getFirstName());
        Mockito.verify(repository, Mockito.times(1)).persist(any(Person.class));
    }

    @Test
    void create_data_not_changed() {
        Person entityUnderTest = Person.builder()
            .dateOfBirth(LocalDate.of(1995, Month.AUGUST, 20))
            .build();

        service.create(entityUnderTest);
        assertEquals(LocalDate.of(1995, Month.AUGUST, 20), entityUnderTest.getDateOfBirth());
        assertNull(entityUnderTest.getFirstName());
        Mockito.verify(repository, Mockito.times(1)).persist(any(Person.class));
    }

    @Test
    void read() {

        String firstName = faker.name().firstName();

        Mockito.when(repository.listAll()).thenReturn(List.of(
            Person.builder()
                .firstName(firstName)
                .dateOfBirth(LocalDate.now())
                .build()
        ));

        List<Person> result = service.read();
        assertEquals(1, result.size());
        assertEquals(firstName, result.get(0).getFirstName());
        assertNotNull(result.get(0).getDateOfBirth());

        Mockito.verify(repository, Mockito.times(1)).listAll();
    }

    @Test
    void readOne() {

        String firstName = faker.name().firstName();

        Mockito.when(repository.findById(any(ObjectId.class))).thenReturn(Person.builder()
            .firstName(firstName)
            .dateOfBirth(LocalDate.now())
            .build());

        Person result = service.readOne(ObjectId.get());
        assertEquals(firstName, result.getFirstName());

    }

    @Test
    void update() {
        Person entityUnderTest = Person.builder()
            .id(ObjectId.get())
            .firstName(faker.name().firstName())
            .dateOfBirth(LocalDate.now())
            .build();

        service.update(entityUnderTest);
        Mockito.verify(repository, Mockito.times(1)).update(any(Person.class));

    }

    @Test
    void delete() {
        service.delete(ObjectId.get());
        Mockito.verify(repository, Mockito.times(1)).deleteById(any(ObjectId.class));
    }
}