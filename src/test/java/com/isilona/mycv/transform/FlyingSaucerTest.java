package com.isilona.mycv.transform;

import static com.itextpdf.text.pdf.BaseFont.EMBEDDED;
import static com.itextpdf.text.pdf.BaseFont.IDENTITY_H;
import static org.thymeleaf.templatemode.TemplateMode.HTML;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.dto.OccupationDto;
import com.isilona.mycv.data.entity.Knowledge;
import com.isilona.mycv.data.entity.Occupation;
import com.isilona.mycv.data.entity.Occupation.Company;
import com.isilona.mycv.data.entity.OccupationType;
import com.isilona.mycv.data.entity.Person;
import com.isilona.mycv.data.entity.Person.SocialLink;
import com.isilona.mycv.data.entity.Person.Title;
import com.isilona.mycv.data.mapper.OccupationMapper;
import io.quarkus.test.junit.QuarkusTest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.FileSystems;
import java.time.ZoneId;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

@QuarkusTest
class FlyingSaucerTest extends BaseFaker {

    private static final String OUTPUT_FILE = "profile.pdf";
    private static final String UTF_8 = "UTF-8";

    @Inject
    OccupationMapper occupationMapper;

    @Test
    void generatePdf() throws Exception {

        // We set-up a Thymeleaf rendering engine. All Thymeleaf templates
        // are HTML-based files located under "src/test/resources". Beside
        // of the main HTML file, we also have partials like a footer or
        // a header. We can re-use those partials in different documents.
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setPrefix("/");
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode(HTML);
        templateResolver.setCharacterEncoding(UTF_8);

        TemplateEngine templateEngine = new TemplateEngine();
        templateEngine.setTemplateResolver(templateResolver);

        // The data in our Thymeleaf templates is not hard-coded. Instead,
        // we use placeholders in our templates. We fill these placeholders
        // with actual data by passing in an object. In this example, we will
        // write a letter to "John Doe".
        //
        // Note that we could also read this data from a JSON file, a database
        // a web service or whatever.
        List<OccupationDto> experienceData = exampleExperienceData();
        List<OccupationDto> educationData = exampleEducationData();
        Knowledge knowledgeData = exampleKnowledgeData();
        Person personalData = examplePersonalData();

        Context context = new Context();
        context.setVariable("experienceData", experienceData);
        context.setVariable("educationData", educationData);
        context.setVariable("knowledgeData", knowledgeData);
        context.setVariable("personalData", personalData);

        // Flying Saucer needs XHTML - not just normal HTML. To make our life
        // easy, we use JTidy to convert the rendered Thymeleaf template to
        // XHTML. Note that this might not work for very complicated HTML. But
        // it's good enough for a simple letter.
        String renderedHtmlContent = templateEngine.process("template", context);
        String xHtml = convertToXhtml(renderedHtmlContent);

        ITextRenderer renderer = new ITextRenderer();
        renderer.getFontResolver().addFont("META-INF/resources/pdfTemplates/Code39.ttf", IDENTITY_H, EMBEDDED);

        // FlyingSaucer has a working directory. If you run this main, the working directory
        // will be the root folder of your project. However, all files (HTML, CSS, etc.) are
        // located under "/src/main/resources". So we want to use this folder as the working
        // directory.
        String baseUrl = FileSystems
            .getDefault()
            .getPath("src", "main", "resources", "META-INF", "resources", "pdfTemplates")
            .toUri()
            .toURL()
            .toString();
        renderer.setDocumentFromString(xHtml, baseUrl);
        renderer.layout();

        // And finally, we create the PDF:
        OutputStream outputStream = new FileOutputStream(OUTPUT_FILE);
        renderer.createPDF(outputStream);
        outputStream.close();
    }


    private String convertToXhtml(String html) {
        Tidy tidy = new Tidy();
        tidy.setInputEncoding(UTF_8);
        tidy.setOutputEncoding(UTF_8);
        tidy.setXHTML(true);
        ByteArrayInputStream inputStream = new ByteArrayInputStream(html.getBytes(StandardCharsets.UTF_8));
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        tidy.parseDOM(inputStream, outputStream);
        return outputStream.toString(StandardCharsets.UTF_8);
    }

    private List<OccupationDto> exampleExperienceData() {

        List<OccupationDto> result = new LinkedList<>();

        int dataSize = 8;
        for (int i = 0; i < dataSize; i++) {
            Occupation occupation = Occupation.builder()
                .id(ObjectId.get())
                .company(Company.builder()
                    .name(faker.company().name())
                    .url(faker.company().url())
                    .build()
                )
                .description(faker.lorem().paragraph())
                .startDate(faker.date().past(300, 200, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .endDate(faker.date().past(199, 100, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .position(faker.job().title())
                .type(OccupationType.WRK)
                .build();

            result.add(occupationMapper.toDto(occupation));
        }

        return result;
    }

    private List<OccupationDto> exampleEducationData() {

        List<OccupationDto> result = new LinkedList<>();

        int dataSize = 2;
        for (int i = 0; i < dataSize; i++) {
            Occupation occupation = Occupation.builder()
                .id(ObjectId.get())
                .company(Company.builder()
                    .name(faker.educator().university())
                    .url(faker.company().url())
                    .build()
                )
                .description(faker.lorem().paragraph())
                .startDate(faker.date().past(300, 200, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .endDate(faker.date().past(199, 100, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .position(faker.educator().course())
                .type(OccupationType.EDU)
                .build();

            result.add(occupationMapper.toDto(occupation));
        }

        return result;
    }

    private Knowledge exampleKnowledgeData() {
        return Knowledge.builder().skillBuckets(
            Map.of(
                "OS", List.of("Windows", "Linux (Ubuntu)", "MacOS"),
                "Servlet containers", List.of("Tomcat", "Jetty"),
                "Frameworks", List.of("Hibernate", "Spring(Boot)", "Quarkus", "Selenium"),
                "Database", List.of("Oracle", "PostgreSQL", "MongoDB"),
                "CI/CD", List.of("BitBucket Pipelines", "Travis CI"),
                "Markup", List.of("XML", "XSL(T)", "HTML"),
                "Development Tools", List.of("IntelliJ IDEA", "Seeburger BIC Mapping Designe"),
                "WEB Technologies", List.of("JSP", "JavaScript", "Angular", "JQuery"),
                "Test & Quality", List.of("SonarQube", "TestNG", "JUnit"),
                "AWS", List.of("IAM", "ECS", "CloudFormation", "S3", "ECR"))
        ).build();
    }

    private Person examplePersonalData() {
        return Person.builder()
            .firstName(faker.name().firstName())
            .lastName(faker.name().lastName())
            .title(Title.builder().base(faker.job().position()).build())
            .socialLinks(Set.of(
                SocialLink.builder().faClass("fab fa-medium").url(faker.internet().url()).build(),
                SocialLink.builder().faClass("fab fa-github").url(faker.internet().url()).build(),
                SocialLink.builder().faClass("fab fa-linkedin").url(faker.internet().url()).build(),
                SocialLink.builder().faClass("fab fa-bitbucket").url(faker.internet().url()).build(),
                SocialLink.builder().faClass("fas fa-envelope").url(faker.internet().url()).build()
            ))
            .build();
    }
}
