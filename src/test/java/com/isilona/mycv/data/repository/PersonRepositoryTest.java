package com.isilona.mycv.data.repository;

import static org.junit.jupiter.api.Assertions.assertTrue;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.entity.Person;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import java.time.LocalDate;
import javax.inject.Inject;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestProfile(RepositoryTestProfile.class)
class PersonRepositoryTest extends BaseFaker {

    @Inject
    PersonRepository personRepository;

    @Test
    void create() {
        Person entityUnderTest = Person.builder()
            .firstName(faker.name().firstName())
            .dateOfBirth(LocalDate.now())
            .build();

        personRepository.persist(entityUnderTest);

        assertTrue(personRepository.listAll().size() > 0);
    }

}