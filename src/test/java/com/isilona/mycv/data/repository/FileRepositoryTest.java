package com.isilona.mycv.data.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.entity.File;
import com.isilona.mycv.data.entity.FileType;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestProfile(RepositoryTestProfile.class)
class FileRepositoryTest extends BaseFaker {

    @Inject
    FileRepository repository;

    @Test
    void findOneByPersonAndType() {

        ObjectId personId = ObjectId.get();
        FileType fileType = FileType.PROFILE_IMG;

        assertNull(repository.findOneByPersonAndType(personId, fileType));

        File entityUnderTest = File.builder()
            .path(faker.file().fileName())
            .personId(personId)
            .type(fileType)
            .build();

        repository.persist(entityUnderTest);

        assertNotNull(repository.findOneByPersonAndType(personId, fileType));
    }
}