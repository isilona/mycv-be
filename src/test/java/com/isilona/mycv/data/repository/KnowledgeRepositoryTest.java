package com.isilona.mycv.data.repository;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.entity.Knowledge;
import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestTransaction
@TestProfile(RepositoryTestProfile.class)
class KnowledgeRepositoryTest extends BaseFaker {

    @Inject
    KnowledgeRepository repository;

    @Test
    @TestTransaction
    void findByPerson() {

        ObjectId personId = ObjectId.get();

        assertNull(repository.findByPerson(personId));

        Knowledge entityUnderTest = Knowledge.builder()
            .personId(personId)
            .skillBuckets(Map.of(faker.lorem().characters(), List.of(faker.lorem().characters())))
            .build();

        repository.persist(entityUnderTest);

        assertNotNull(repository.findByPerson(personId));
    }
}