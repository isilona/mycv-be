package com.isilona.mycv.data.repository;

import io.quarkus.test.junit.QuarkusTestProfile;

public class RepositoryTestProfile implements QuarkusTestProfile {


    @Override
    public String getConfigProfile() {
        return "repository-test";
    }


}
