package com.isilona.mycv.data.repository;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.entity.Occupation;
import com.isilona.mycv.data.entity.Occupation.Company;
import com.isilona.mycv.data.entity.OccupationType;
import io.quarkus.test.TestTransaction;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import java.time.ZoneId;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@QuarkusTest
@TestTransaction
@TestInstance(Lifecycle.PER_CLASS)
@TestProfile(RepositoryTestProfile.class)
class OccupationRepositoryTest extends BaseFaker {

    static ObjectId personId;

    @Inject
    OccupationRepository repository;

    @BeforeAll
    void setup() {
        personId = ObjectId.get();

        assertEquals(0, repository.findAllByPersonAndType(personId, OccupationType.EDU).size());
        assertEquals(0, repository.findAllByPersonAndType(personId, OccupationType.WRK).size());

        Occupation work = Occupation.builder()
            .id(ObjectId.get())
            .company(Company.builder()
                .name(faker.company().name())
                .url(faker.company().url())
                .build()
            )
            .description(faker.lorem().paragraph())
            .startDate(faker.date().past(300, 200, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .endDate(faker.date().past(199, 100, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .position(faker.job().title())
            .type(OccupationType.WRK)
            .personId(personId)
            .build();

        Occupation edu = Occupation.builder()
            .id(ObjectId.get())
            .company(Company.builder()
                .name(faker.company().name())
                .url(faker.company().url())
                .build()
            )
            .description(faker.lorem().paragraph())
            .startDate(faker.date().past(300, 200, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .endDate(faker.date().past(199, 100, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .position(faker.job().title())
            .type(OccupationType.EDU)
            .personId(personId)
            .build();

        repository.persist(work);
        repository.persist(edu);
    }

    @Test
    @TestTransaction
    void findAllByPersonAndType() {
        List<Occupation> work = repository.findAllByPersonAndType(personId, OccupationType.WRK);
        assertEquals(1, work.size());
        List<Occupation> edu = repository.findAllByPersonAndType(personId, OccupationType.EDU);
        assertEquals(1, edu.size());
    }

    @Test
    void findAllByPerson() {
        assertEquals(2, repository.findAllByPerson(personId).size());
    }
}