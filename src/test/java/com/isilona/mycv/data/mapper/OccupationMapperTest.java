package com.isilona.mycv.data.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.isilona.mycv.BaseFaker;
import com.isilona.mycv.data.dto.OccupationDto;
import com.isilona.mycv.data.entity.Occupation;
import com.isilona.mycv.data.entity.Occupation.Company;
import com.isilona.mycv.data.entity.OccupationType;
import io.quarkus.test.junit.QuarkusTest;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

@QuarkusTest
class OccupationMapperTest extends BaseFaker {

    @Inject
    OccupationMapper mapper;

    @Test
    void toDto_Default() {

        Occupation actual = Occupation.builder()
            .id(ObjectId.get())
            .company(Company.builder()
                .name(faker.company().name())
                .url(faker.company().url())
                .build()
            )
            .description(faker.lorem().paragraph())
            .startDate(faker.date().past(300, 200, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .endDate(faker.date().past(199, 100, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
            .position(faker.job().title())
            .type(OccupationType.WRK)
            .build();

        OccupationDto expected = mapper.toDto(actual);

        assertEquals(expected.getId(), actual.getId().toString());
        assertEquals(expected.getPosition(), actual.getPosition());
        assertEquals(expected.getCompany().getName(), actual.getCompany().getName());
        assertEquals(expected.getCompany().getUrl(), actual.getCompany().getUrl());
        assertEquals(expected.getStartDate(), actual.getStartDate().format(DateTimeFormatter.ofPattern("MMM yyyy")));
        assertEquals(expected.getType(), OccupationType.WRK.name());
        assertEquals(expected.getDescription(), actual.getDescription());

    }

    @Test
    void toDto_test_currently_occupied() {
        Occupation actual = Occupation.builder().build();
        OccupationDto transformed = mapper.toDto(actual);
        assertEquals("Present", transformed.getEndDate());
    }

    @Test
    void toDto_test_date_transformation() {
        Occupation actual = Occupation.builder()
            .startDate(LocalDate.parse("2016-08-16"))
            .endDate(LocalDate.parse("2018-01-21"))
            .build();

        OccupationDto transformed = mapper.toDto(actual);

        assertEquals("Aug 2016", transformed.getStartDate());
        assertEquals("Jan 2018", transformed.getEndDate());
    }

}