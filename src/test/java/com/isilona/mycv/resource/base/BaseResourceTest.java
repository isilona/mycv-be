package com.isilona.mycv.resource.base;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.isilona.mycv.BaseFaker;

public class BaseResourceTest extends BaseFaker {

    protected static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    protected static String getAsJson(Object data) throws JsonProcessingException {
        return OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(data);
    }

}
