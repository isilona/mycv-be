package com.isilona.mycv.resource.base;

import io.quarkus.test.junit.QuarkusTestProfile;

public class MockProfile implements QuarkusTestProfile {


    @Override
    public String getConfigProfile() {
        return "dev";
    }


}
