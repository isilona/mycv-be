package com.isilona.mycv.resource.base;

import io.quarkus.test.security.TestSecurity;

@TestSecurity(authorizationEnabled = false)
public class BaseAuthorizationDisabledResourceTest extends BaseResourceTest {


}
