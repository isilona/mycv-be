package com.isilona.mycv.resource;

import static io.restassured.RestAssured.given;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isilona.mycv.data.entity.Knowledge;
import com.isilona.mycv.resource.base.BaseAuthorizationDisabledResourceTest;
import com.isilona.mycv.resource.base.MockProfile;
import com.isilona.mycv.service.KnowledgeService;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;
import java.util.List;
import java.util.Map;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestHTTPEndpoint(KnowledgeResource.class)
@TestProfile(MockProfile.class)
class KnowledgeResourceTest extends BaseAuthorizationDisabledResourceTest {


    @InjectMock
    KnowledgeService service;

    @Test
    void create() throws JsonProcessingException {
        given()
            .body(getAsJson(Knowledge.builder()
                .skillBuckets(Map.of(faker.lorem().characters(), List.of(faker.lorem().characters())))
                .build())
            )
            .contentType(ContentType.JSON)
            .when().post()
            .then()
            .statusCode(204);
    }

    @Test
    void findAllByPerson() {

        ObjectId personId = ObjectId.get();
        Mockito.when(service.findByPerson(personId)).thenReturn(
            Knowledge.builder()
                .skillBuckets(Map.of(faker.lorem().characters(), List.of(faker.lorem().characters())))
                .build()
        );

        given()
            .pathParam("personId", personId.toString())
            .when().get("/{personId}")
            .then()
            .statusCode(200);


    }

    @Test
    void update() throws JsonProcessingException {

        given()
            .body(getAsJson(Knowledge.builder()
                .skillBuckets(Map.of(faker.lorem().characters(), List.of(faker.lorem().characters())))
                .build()))
            .contentType(ContentType.JSON)
            .when().put()
            .then()
            .statusCode(204);

    }

    @Test
    void delete() {
        ObjectId id = ObjectId.get();
        given()
            .pathParam("id", id.toString())
            .contentType(ContentType.JSON)
            .when().delete("/{id}")
            .then()
            .statusCode(204);
    }
}