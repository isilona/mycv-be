package com.isilona.mycv.resource;

import static com.isilona.mycv.data.entity.FileType.PROFILE_IMG;
import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.containsString;

import com.amazonaws.services.s3.model.S3Object;
import com.isilona.mycv.data.entity.File;
import com.isilona.mycv.resource.base.BaseAuthorizationDisabledResourceTest;
import com.isilona.mycv.resource.base.MockProfile;
import com.isilona.mycv.service.FileService;
import com.isilona.mycv.service.S3Service;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestHTTPEndpoint(FileResource.class)
@TestProfile(MockProfile.class)
class FileResourceTest extends BaseAuthorizationDisabledResourceTest {

    @InjectMock
    FileService service;

    @InjectMock
    S3Service s3Service;

    @Test
    void testCreateEndpoint() throws IOException {

        ObjectId personId = ObjectId.get();
        ObjectId objectId = ObjectId.get();

        //  mock File Entity
        File mockedFileEntity = File.builder()
            .type(PROFILE_IMG)
            .personId(personId)
            .path(faker.file().fileName())
            .id(objectId)
            .build();
        Mockito.when(service.findOneByPersonAndTypeOrCreateNew(personId, null, null)).thenReturn(mockedFileEntity);

        java.io.File tempFile = java.io.File.createTempFile("tempfile", ".tmp");
        byte[] tempFileBytes = FileUtils.readFileToByteArray(tempFile);
        // mock S3File
        S3Object mockedS3File = new S3Object();
        String mockedS3FileContent = faker.lorem().sentence();
        mockedS3File.setObjectContent(new ByteArrayInputStream(mockedS3FileContent.getBytes()));

        given()
            .contentType("multipart/form-data")
            .pathParam("personId", personId.toString())
            .multiPart("data", tempFile)
            .when().post("/{personId}")
            .then()
            .statusCode(204);

        Mockito.verify(s3Service, Mockito.times(1)).putObject(mockedFileEntity, tempFileBytes);

    }

    @Test
    void testReadEndpoint() {
        ObjectId personId = ObjectId.get();
        ObjectId objectId = ObjectId.get();

        //  mock File Entity
        File mockedFileEntity = File.builder()
            .type(PROFILE_IMG)
            .personId(personId)
            .path(faker.file().fileName())
            .id(objectId)
            .build();
        Mockito.when(service.findOneByPersonAndType(personId, PROFILE_IMG)).thenReturn(mockedFileEntity);

        // mock S3File
        S3Object mockedS3File = new S3Object();
        String mockedS3FileContent = faker.lorem().sentence();
        mockedS3File.setObjectContent(new ByteArrayInputStream(mockedS3FileContent.getBytes()));
        Mockito.when(s3Service.getObject(mockedFileEntity)).thenReturn(mockedS3File);

        given()
            .pathParam("personId", personId.toString())
            .pathParam("fileType", PROFILE_IMG)
            .when().get("/{personId}/{fileType}")
            .then()
            .statusCode(200)
            .body(containsString(mockedS3FileContent));
    }
}