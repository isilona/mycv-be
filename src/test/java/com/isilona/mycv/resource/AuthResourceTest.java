package com.isilona.mycv.resource;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.eq;

import com.isilona.mycv.data.dto.Token;
import com.isilona.mycv.resource.base.BaseAuthorizationDisabledResourceTest;
import com.isilona.mycv.resource.base.MockProfile;
import com.isilona.mycv.service.AuthService;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.eclipse.microprofile.rest.client.inject.RestClient;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestHTTPEndpoint(AuthResource.class)
@TestProfile(MockProfile.class)
class AuthResourceTest extends BaseAuthorizationDisabledResourceTest {


    @InjectMock
    @RestClient
    AuthService service;

    @Test
    void testDeleteEndpoint() {

        RestAssured.registerParser("application/json", Parser.JSON);

        String grantType = "password";
        String scope = "openid";
        String password = faker.internet().password();
        String username = faker.name().username();

        Mockito.when(service.getToken(eq(grantType), eq(scope), eq(username), eq(password))).thenReturn(
            Token.builder()
                .accessToken(
                    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")
                .expiresIn(3600)
                .idToken(
                    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c")
                .scope("openid")
                .tokenType("Bearer")
                .build()
        );

        given()
            .queryParam("password", password)
            .queryParam("username", username)
            .contentType(ContentType.JSON)
            .when().post("/login")
            .then()
            .body("access_token", equalTo(
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"));
    }

}