package com.isilona.mycv.resource;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isilona.mycv.data.entity.Template;
import com.isilona.mycv.resource.base.BaseAuthorizationDisabledResourceTest;
import com.isilona.mycv.resource.base.MockProfile;
import com.isilona.mycv.service.TemplateService;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;
import java.util.List;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestHTTPEndpoint(TemplateResource.class)
@TestProfile(MockProfile.class)
class TemplateResourceTest extends BaseAuthorizationDisabledResourceTest {

    @InjectMock
    TemplateService service;

    @Test
    void testCreateEndpoint() throws JsonProcessingException {

        given()
            .body(getAsJson(Template.builder()
                .value(getLorem(10))
                .build())
            )
            .contentType(ContentType.JSON)
            .when().post()
            .then()
            .statusCode(204);
    }

    @Test
    void testReadEndpoint() {
        String templateValue = getLorem(10);
        Mockito.when(service.read()).thenReturn(List.of(
            Template.builder()
                .value(templateValue)
                .build()
        ));

        given()
            .when().get()
            .then()
            .statusCode(200)
            .body("[0].value", equalTo(templateValue));
    }

    @Test
    void testReadOneEndpoint() {
        String templateValue = getLorem(10);
        ObjectId id = ObjectId.get();

        Mockito.when(service.readOne(id)).thenReturn(
            Template.builder()
                .value(templateValue)
                .build()
        );

        given()
            .pathParam("id", id.toString())
            .when().get("/{id}")
            .then()
            .statusCode(200)
            .body("value", equalTo(templateValue));

    }

    @Test
    void testUpdateEndpoint() throws JsonProcessingException {
        String templateValue = getLorem(10);
        given()
            .body(getAsJson(
                Template.builder()
                    .value(templateValue)
                    .build()
            ))
            .contentType(ContentType.JSON)
            .when().put()
            .then()
            .statusCode(204);

    }

    @Test
    void testDeleteEndpoint() {
        ObjectId id = ObjectId.get();
        given()
            .pathParam("id", id.toString())
            .contentType(ContentType.JSON)
            .when().delete("/{id}")
            .then()
            .statusCode(204);

    }

}