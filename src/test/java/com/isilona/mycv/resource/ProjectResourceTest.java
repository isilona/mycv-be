package com.isilona.mycv.resource;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isilona.mycv.data.entity.Project;
import com.isilona.mycv.data.entity.Project.Gain;
import com.isilona.mycv.resource.base.BaseAuthorizationDisabledResourceTest;
import com.isilona.mycv.resource.base.MockProfile;
import com.isilona.mycv.service.ProjectService;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;
import java.util.List;
import java.util.Set;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestHTTPEndpoint(ProjectResource.class)
@TestProfile(MockProfile.class)
class ProjectResourceTest extends BaseAuthorizationDisabledResourceTest {

    @InjectMock
    ProjectService service;

    @Test
    void testCreateEndpoint() throws JsonProcessingException {

        given()
            .body(getAsJson(Project.builder()
                .name(getProjectName())
                .gains(Set.of(
                    Gain.builder()
                        .description(getLorem(10))
                        .build()))
                .build())
            )
            .contentType(ContentType.JSON)
            .when().post()
            .then()
            .statusCode(204);
    }

    @Test
    void testReadEndpoint() {
        String projectName = getProjectName();
        Mockito.when(service.read()).thenReturn(List.of(
            Project.builder()
                .name(projectName)
                .gains(Set.of(
                    Gain.builder()
                        .description(getLorem(10))
                        .build()))
                .build()
        ));

        given()
            .when().get()
            .then()
            .statusCode(200)
            .body("[0].name", equalTo(projectName))
            .body("[0].gains[0].description", notNullValue());
    }

    @Test
    void testReadOneEndpoint() {
        String projectName = getProjectName();
        ObjectId id = ObjectId.get();

        Mockito.when(service.readOne(id)).thenReturn(
            Project.builder()
                .name(projectName)
                .build()
        );

        given()
            .pathParam("id", id.toString())
            .when().get("/{id}")
            .then()
            .statusCode(200)
            .body("name", equalTo(projectName));

    }

    @Test
    void testUpdateEndpoint() throws JsonProcessingException {
        String projectName = getProjectName();
        given()
            .body(getAsJson(
                Project.builder()
                    .name(projectName)
                    .build())
            )
            .contentType(ContentType.JSON)
            .when().put()
            .then()
            .statusCode(204);

    }

    @Test
    void testDeleteEndpoint() {
        ObjectId id = ObjectId.get();
        given()
            .pathParam("id", id.toString())
            .contentType(ContentType.JSON)
            .when().delete("/{id}")
            .then()
            .statusCode(204);

    }

}