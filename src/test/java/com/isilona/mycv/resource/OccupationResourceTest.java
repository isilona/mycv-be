package com.isilona.mycv.resource;

import static io.restassured.RestAssured.given;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isilona.mycv.data.entity.Occupation;
import com.isilona.mycv.data.entity.Occupation.Company;
import com.isilona.mycv.resource.base.BaseAuthorizationDisabledResourceTest;
import com.isilona.mycv.resource.base.MockProfile;
import com.isilona.mycv.service.OccupationService;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;

@QuarkusTest
@TestHTTPEndpoint(OccupationResource.class)
@TestProfile(MockProfile.class)
class OccupationResourceTest extends BaseAuthorizationDisabledResourceTest {

    @InjectMock
    OccupationService service;

    @Test
    void testCreateEndpoint() throws JsonProcessingException {

        String position = faker.job().position();

        given()
            .body(getAsJson(
                Occupation.builder()
                    .position(position)
                    .company(Company.builder()
                        .name(getLorem(10))
                        .build())
                    .build())
            )
            .contentType(ContentType.JSON)
            .when().post()
            .then()
            .statusCode(204);
    }

    @Test
    void testUpdateEndpoint() throws JsonProcessingException {
        String position = faker.job().position();
        given()
            .body(getAsJson(
                Occupation.builder()
                    .position(position)
                    .build())
            )
            .contentType(ContentType.JSON)
            .when().put()
            .then()
            .statusCode(204);

    }

    @Test
    void testDeleteEndpoint() {
        ObjectId id = ObjectId.get();
        given()
            .pathParam("id", id.toString())
            .contentType(ContentType.JSON)
            .when().delete("/{id}")
            .then()
            .statusCode(204);

    }

}