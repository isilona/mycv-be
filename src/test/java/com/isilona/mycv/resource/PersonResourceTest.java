package com.isilona.mycv.resource;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.isilona.mycv.data.entity.Person;
import com.isilona.mycv.data.entity.Person.Address;
import com.isilona.mycv.data.entity.Person.Contact;
import com.isilona.mycv.data.entity.Person.SocialLink;
import com.isilona.mycv.resource.base.BaseAuthorizationDisabledResourceTest;
import com.isilona.mycv.resource.base.MockProfile;
import com.isilona.mycv.service.PersonService;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.TestProfile;
import io.quarkus.test.junit.mockito.InjectMock;
import io.restassured.http.ContentType;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

@QuarkusTest
@TestHTTPEndpoint(PersonResource.class)
@TestProfile(MockProfile.class)
class PersonResourceTest extends BaseAuthorizationDisabledResourceTest {

    @InjectMock
    PersonService service;

    @Test
    void testCreateEndpoint() throws JsonProcessingException {

        given()
            .body(getAsJson(
                Person.builder()
                    .firstName(faker.name().firstName())
                    .lastName(faker.name().lastName())
                    .address(Address.builder()
                        .country(faker.country().name())
                        .build())
                    .contact(Contact.builder()
                        .email(faker.internet().emailAddress())
                        .build())
                    .socialLinks(Set.of(SocialLink.builder()
                        .faClass(getLorem(10))
                        .build()))
                    .build())
            )
            .contentType(ContentType.JSON)
            .when().post()
            .then()
            .statusCode(204);
    }

    @Test
    void testReadEndpoint() {
        String firstName = faker.name().firstName();
        Mockito.when(service.read()).thenReturn(List.of(
            Person.builder()
                .firstName(firstName)
                .lastName(faker.name().lastName())
                .address(Address.builder()
                    .country(faker.country().name())
                    .build())
                .contact(Contact.builder()
                    .email(faker.internet().emailAddress())
                    .build())
                .socialLinks(Set.of(SocialLink.builder()
                    .faClass(getLorem(10))
                    .build()))
                .build()
        ));

        given()
            .when().get()
            .then()
            .statusCode(200)
            .body("[0].firstName", equalTo(firstName))
            .body("[0].address.country", notNullValue())
            .body("[0].contact.email", notNullValue())
            .body("[0].socialLinks.url", notNullValue());
    }

    @Test
    void testReadOneEndpoint() {

        ObjectId id = ObjectId.get();
        String firstName = faker.name().firstName();

        Mockito.when(service.readOne(id)).thenReturn(
            Person.builder()
                .firstName(firstName)
                .dateOfBirth(LocalDate.now())
                .build()
        );

        given()
            .pathParam("id", id.toString())
            .when().get("/{id}")
            .then()
            .statusCode(200)
            .body("firstName", equalTo(firstName));

    }

    @Test
    void testUpdateEndpoint() throws JsonProcessingException {
        String firstName = faker.name().firstName();
        String lastName = faker.name().lastName();

        given()
            .body(getAsJson(
                Person.builder()
                    .firstName(firstName)
                    .lastName(lastName)
                    .build())
            )
            .contentType(ContentType.JSON)
            .when().put()
            .then()
            .statusCode(204);

    }

    @Test
    void testDeleteEndpoint() {
        ObjectId id = ObjectId.get();
        given()
            .pathParam("id", id.toString())
            .contentType(ContentType.JSON)
            .when().delete("/{id}")
            .then()
            .statusCode(204);

    }

}