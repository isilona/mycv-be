package com.isilona.mycv;

import com.github.javafaker.Faker;

public class BaseFaker {

    protected static final Faker faker = new Faker();

    protected String getLorem(Integer numberOfLetters) {
        return faker.lorem().fixedString(numberOfLetters);
    }

    protected String getProjectName() {
        return faker.pokemon().name();
    }


}
